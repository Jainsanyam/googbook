export class Movie {
  name: string;
  artist: string;

  image: string;
}

export var moviedb: Movie[] = [
  {
    name: 'Harry Potter',
    author: 'J.K.Rowling',
    description:
      'Harry Potter is a series of fantasy novels written by British author J. K. Rowling. The novels c.',
    image: 'https://images.gr-assets.com/polls/1380398422p7/91309.jpg',
  },
  {
    name: 'War and Peace',
    author: 'Leo Tolstoy',
    description:
      'War and Peace is a novel by the Russian author Leo Tolstoy. It is regarded as a central work of worl.',
    image:
      'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1384803290l/18626867.jpg',
  },
  {
    name: 'Malgudi Days',
    author: 'RK Narayan',
    description:
      'Malgudi Days is a collection of short stories by R. K. Narayan published in 1943 by Indian Thought Pub.',
    image:
      'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1455574981l/14082.jpg',
  },
  {
    name: 'Gitanjali',
    author: 'RK Tagore',
    description:
      'Gitanjali is a collection of poems by the Bengali poet Rabindranath Tagore. Tagore received the Nobel Priz',
    image:
      'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1385265973l/18922254.jpg',
  },
];
