import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { BooksNetService } from "./../books-net.service";
@Component({
    selector: "app-google-books-store",
    templateUrl: "./google-books-store.component.html",
    styleUrls: ["./google-books-store.component.css"],
})
export class GoogleBooksStoreComponent implements OnInit {
    bookName: string = "";
    mainContent: boolean = false;
    check: boolean = true;
    book: string = "";
    currPage: string;
    bookData: any[] = [];
    startIndex = 0;
    maxResult = 8;
    si = 0;
    maxre = 8;
    langauageArray = ["English", "French", "Hindi"];
    filter = ["Full Volume", "Free Google e-books", "Paid Google e-books"];
    languageStructure = null;
    filterStructure = null;
    lana = "";
    filteredData = "";
    constructor(
        private bookService: BooksNetService,
        private route: ActivatedRoute,
        private router: Router
    ) {}
    baseUrl = "https://www.googleapis.com/books/v1/volumes?q=";
    url = "";
    homeImage = [
        {
            image:
                "https://www.thebetterindia.com/wp-content/uploads/2017/12/books-1.jpg",
        },
    ];
    ngOnInit() {
        this.route.paramMap.subscribe((params) => {
            this.book = params.get("bookName");
            console.log(this.book);
            this.makeStructure();
        });
        this.route.queryParamMap.subscribe((params) => {
            console.log(params);
            this.bookName = params.get("searchText");
            this.makeStructure();
        });
        this.makeStruct();
    }
    makeStruct() {
        this.languageStructure = {
            language: this.langauageArray,
            selected: this.lana ? this.lana : "",
        };
        this.filterStructure = {
            filter: this.filter,
            selected: this.filteredData ? this.filteredData : "",
        };
        console.log(this.languageStructure);
        console.log(this.filterStructure);
    }
    optChange() {
        this.showPage();

        this.makeStructure();
    }

    makeUrl() {
        if (this.bookName != "") {
            this.url =
                this.baseUrl +
                "searchText=" +
                this.bookName +
                "&startIndex=" +
                this.startIndex +
                "&maxResults=" +
                this.maxResult +
                "&";
        }
        if (this.lana != "") {
            this.url = this.url + "langRestrict=" + this.lana + "&";
        }
        if (this.filteredData != "") {
            this.url = this.url + "filter=" + this.filteredData;
        }
    }
    searchBook() {
        this.check = false;
        this.mainContent = true;
        // this.makeStructure();
    }
    makeStructure() {
        this.makeUrl();

        this.bookService.getData(this.url).subscribe((resp) => {
            let data = resp.items;
            this.bookData = data;
            console.log(this.bookData);

            this.showPage();
        });
    }
    previous() {
        if (this.startIndex == 0) {
            this.startIndex = 0;
        } else {
            this.startIndex -= 8;
            this.maxre -= 8;
        }
        this.makeStructure();

        this.showPage();
    }
    next() {
        this.startIndex += 8;
        this.maxre += 8;
        this.makeStructure();

        this.showPage();
    }
    showPage() {
        let newJSonArray = [];

        if (this.bookName !== "") {
            newJSonArray.push(
                { startIndex: this.startIndex },
                { maxResults: this.maxResult },
                { searchText: this.bookName }
            );
        }

        if (this.languageStructure.selected) {
            if (this.languageStructure.selected == "English") {
                this.lana = "en";
            } else if (this.languageStructure.selected == "Hindi") {
                this.lana = "hi";
            } else if (this.languageStructure.selected == "French") {
                this.lana = "fr";
            }
            newJSonArray.push({ langRestrict: this.lana });
        }
        if (this.filterStructure.selected) {
            if (this.filterStructure.selected == "Full Volume") {
                this.filteredData = "full";
            } else if (this.filterStructure.selected == "Free Google e-books") {
                this.filteredData = "free-ebooks";
            } else if (this.filterStructure.selected == "Paid Google e-books") {
                this.filteredData = "paid-ebooks";
            }
            newJSonArray.push({ filter: this.filteredData });
        }

        let parameter = newJSonArray.reduce((arr, item) =>
            Object.assign(arr, item, {})
        );
        console.log(parameter);
        let path = "books/" + this.bookName;

        if (this.book != "null") {
            this.bookName = this.book;
            let path = "books/" + this.bookName;

            this.router.navigate([path], {
                queryParams: parameter,
            });
            this.searchBook();
        }
    }
}
