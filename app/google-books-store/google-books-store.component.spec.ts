import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleBooksStoreComponent } from './google-books-store.component';

describe('GoogleBooksStoreComponent', () => {
  let component: GoogleBooksStoreComponent;
  let fixture: ComponentFixture<GoogleBooksStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleBooksStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleBooksStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
