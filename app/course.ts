export class Course {
  name: string;
  cities: any[];
}

export var courseList: Course[] = [
  { name: 'India', cities: [' Mumbai'] },
  { name: 'USA', cities: ['Seattle'] },
  { name: 'China', cities: ['Beijing'] },
];
