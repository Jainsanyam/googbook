import { BooksNetService } from "./books-net.service";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { NavComponent } from "./nav/nav.component";
import { GoogleBooksStoreComponent } from "./google-books-store/google-books-store.component";
import { GoogleBooksOptionsComponent } from "./google-books-options/google-books-options.component";

@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        GoogleBooksStoreComponent,
        GoogleBooksOptionsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [BooksNetService],
    bootstrap: [AppComponent],
})
export class AppModule {}
