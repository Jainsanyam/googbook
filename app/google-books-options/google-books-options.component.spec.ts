import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleBooksOptionsComponent } from './google-books-options.component';

describe('GoogleBooksOptionsComponent', () => {
  let component: GoogleBooksOptionsComponent;
  let fixture: ComponentFixture<GoogleBooksOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleBooksOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleBooksOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
