import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "app-google-books-options",
    templateUrl: "./google-books-options.component.html",
    styleUrls: ["./google-books-options.component.css"],
})
export class GoogleBooksOptionsComponent implements OnInit {
    @Input() language;
    @Input() filter;
    @Output() selValue = new EventEmitter();
    constructor() {}

    ngOnInit() {}
    emitChange() {
        this.selValue.emit();
    }
}
