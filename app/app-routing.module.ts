import { GoogleBooksStoreComponent } from "./google-books-store/google-books-store.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    {
        path: "",
        component: GoogleBooksStoreComponent,
    },
    {
        path: "books",
        component: GoogleBooksStoreComponent,
    },
    {
        path: "books/:bookName",
        component: GoogleBooksStoreComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
