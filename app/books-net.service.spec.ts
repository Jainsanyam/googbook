import { TestBed } from '@angular/core/testing';

import { BooksNetService } from './books-net.service';

describe('BooksNetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BooksNetService = TestBed.get(BooksNetService);
    expect(service).toBeTruthy();
  });
});
