import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParamService {
 parameter:string;
  constructor() { }
  getParam(para:string){
      this.parameter = para;
  }
}
